# Source: https://github.com/git-no/jekyll-breadcrumbs
# License: MIT

module Jekyll
  module Drops
    class BreadcrumbItem < Liquid::Drop
      extend Forwardable

      def_delegator :@page, :data
      def_delegator :@page, :url

      def initialize(page, payload)
        @payload = payload
        @page = page
      end

      def title
        @page.data["breadcrumb"] != nil ? @page.data["breadcrumb"] : @page.data["title"]
      end

      def subset
        @page.data["subset"]
      end
    end
  end

  module Breadcrumbs
    @@pages_cache = {}
    def self.build_breadcrumb(page, payload)
      drop = Drops::BreadcrumbItem

      if page.url == "/"
      then
        payload["breadcrumbs"] = [
          drop.new(page, payload)
        ]
      else
        payload["breadcrumbs"] = []
        pth = page.url.split("/")

        pages = [].concat(page.site.pages).concat(page.site.documents)

        0.upto(pth.size - 1) do |int|
          joined_path = pth[0..int].join("/")

          if @@pages_cache[joined_path] == nil
          then
            @@pages_cache[joined_path] = pages.find { |page_| page_.url.chomp("/") == joined_path }
            @@pages_cache[joined_path] = "nomatch" if @@pages_cache[joined_path] == nil
          end

          item = @@pages_cache[joined_path]

          payload["breadcrumbs"] << drop.new(item, payload) if item != "nomatch"
        end
      end
    end
  end

  Jekyll::Hooks.register [:pages, :documents], :pre_render do |page, payload|  # documents are collections, and collections include also posts
    if page['breadcrumbs'] != false and page.site.config['breadcrumbs'] != false
      then
      Breadcrumbs::build_breadcrumb(page, payload)
    end
  end
end
