require 'jekyll'

require 'jekyll-last-modified-at'

require 'jekyll-breadcrumbs/breadcrumbs'
require 'jekyll-plugin-anu-cecs/relativeurl'

Liquid::Template.register_tag('relativeurl', Jekyll::PluginANUCECS::RelativeURL)
